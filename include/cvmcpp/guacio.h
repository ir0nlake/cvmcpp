#pragma once 

#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>

namespace cvmcpp::guacio {

	struct GuacOperation { 
		std::string opcode;
		std::vector<std::string> arguments;
	};

	GuacOperation decode(std::string in);
	std::string encode(GuacOperation op);

}
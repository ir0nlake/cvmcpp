#pragma once

#include <iostream>
#include <string>

#ifdef _DEBUG
	#define CVMCPP_DEBUG_INFO(message) cvmcpp::debug::debug_log(cvmcpp::debug::debug_severity::info, message, __LINE__, __FILE__)
	#define CVMCPP_DEBUG_WARNING(message) cvmcpp::debug::debug_log(cvmcpp::debug::debug_severity::warning, message, __LINE__, __FILE__)
	#define CVMCPP_DEBUG_ERROR(message) cvmcpp::debug::debug_log(cvmcpp::debug::debug_severity::error, message, __LINE__, __FILE__)
#else 
	#define CVMCPP_DEBUG_INFO(message)
	#define CVMCPP_DEBUG_WARNING(message) cvmcpp::debug::debug_log(cvmcpp::debug::debug_severity::error, message, __LINE__, __FILE__)
	#define CVMCPP_DEBUG_ERROR(message) cvmcpp::debug::debug_log(cvmcpp::debug::debug_severity::error, message, __LINE__, __FILE__)
#endif


namespace cvmcpp::debug {
	
	enum debug_severity : int {
		info,
		warning,
		error
	};
	
	void debug_log(debug_severity sever, std::string message, int line, char* file);
	
}
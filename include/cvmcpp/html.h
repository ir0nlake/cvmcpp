#pragma once

#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>

using boost::algorithm::replace_all;

namespace cvmcpp::html {
	std::string unescape(std::string input);
}
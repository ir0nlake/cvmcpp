#pragma once 

// Some TODOs:
// 1. add file upload support (not looking forward to this)

#include <string>
#include <sstream>
#include <cstring>
#include <vector>
#include <iostream>
#include <functional>

// websocketpp includes
#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>

// internal includes
#include <cvmcpp/guacio.h>

typedef websocketpp::client<websocketpp::config::asio_client> ws_client;

namespace cvmcpp {
	
	class VMClient; // Forward declaration for the VMEvent callbacks
	
	/*
		NOTE: the reason the VM client is passed by pointer is to allow multiple
		VM clients to share the same events (without error) if done right (ie. no global variables used.)
	*/
	using VMEvent = std::function<void(VMClient*, guacio::GuacOperation)>;
	
	struct VMEvents {
		VMEvent OnChat;
		VMEvent OnDisplayUpdate;
		VMEvent OnDisplaySizeUpdate;
		VMEvent OnTurnBegin;
		VMEvent OnTurnOver;
	};
	
	class VMClient {
		
	public:
	
		VMClient() {
		}
	
		VMClient(VMEvents ev) {
			// assign events passed
			AssignEvents(ev);
		}
		
		~VMClient(){
			vmthread->join();
		}
	
		void AssignEvents(VMEvents events_toreg);
		
		// Connect to CollabVM Server
		bool Connect(std::string ip);
		
		// Rename and connect to a VM.
		void ConnectVM(std::string name,std::string vm);
		
		// Take a turn on the VM.
		void TakeTurn();
		
		void Rename(std::string new_name);
		
		void SendChat(std::string body);
		
		// Send a Guacamole message to the server manually. 
		void SendEncodedMessage(guacio::GuacOperation op);
	
		bool IsConnected() { return connected; }
		
		// Get the current username.
		std::string GetUsername() { return username; }
	
	private:
			bool connected;
			std::string username;
			VMEvents events;
			
			// ws bullshit
			websocketpp::lib::shared_ptr<websocketpp::lib::thread> vmthread;
			ws_client vmclient;
			websocketpp::connection_hdl vm_connhdl;
			ws_client::connection_ptr con;
			
			void OnConnect(websocketpp::connection_hdl handle);
			void OnDiscon(websocketpp::connection_hdl handle);
			void OnMessage(websocketpp::connection_hdl hdl, ws_client::message_ptr msg);
	};
	
}

// Sample test client using cvmcpp
#if defined(_MSC_VER)
	#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#endif
#include <cvmcpp/client.h>

void OnChatCustom(cvmcpp::VMClient* vm, cvmcpp::guacio::GuacOperation op){\
	std::cout << "hey look what we have a chat message: " << op.arguments[1] << "\n";
}

int main(){
	std::cout << "VM Connection sample" << "\n";
	// Set up the events we want to use.
	cvmcpp::VMEvents ev;
	ev.OnChat = OnChatCustom;
	
	// Connect to the VM server, and assign events to the client.
	cvmcpp::VMClient vm;
	vm.AssignEvents(ev);
	
	vm.Connect("192.151.157.114:6004");
	
	std::this_thread::sleep_for(std::chrono::seconds(1));
	
	// Connect to a VM on the server.
	vm.ConnectVM("cvmcpp_example","vm1");
	
	while(vm.IsConnected()){
		// Nail the main thread while the VM is connected
		std::this_thread::sleep_for(std::chrono::milliseconds(16));
	}
	
	std::cout << "VM server disconnected us, bailing.\n";
	return 0;
}
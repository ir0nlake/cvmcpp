#include <cvmcpp/html.h>

namespace cvmcpp::html {

	// Autistic but it works
	// B-BUT THE O(N^2)!!!1232432
	std::string unescape(std::string input){
		std::string buffer = input;

		replace_all(buffer, "&amp;", "&");
		replace_all(buffer, "&quot;", "\"");
		replace_all(buffer, "&#x27;", "\'");
		replace_all(buffer, "&lt;", "<");
		replace_all(buffer, "&gt;", ">");
		replace_all(buffer, "&#x2F;", "/");
		replace_all(buffer, "&#13;&#10;", "\n");
		
		return buffer;
	}

}
#if defined(_MSC_VER)
	#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#endif
#include <cvmcpp/client.h>
#include <cvmcpp/debug.h>

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

namespace cvmcpp {

	void VMClient::OnMessage(websocketpp::connection_hdl handle, ws_client::message_ptr msg) {
		std::string gmsg = msg->get_payload();
		guacio::GuacOperation oper = guacio::decode(gmsg);
		if(oper.opcode == "nop"){
			SendEncodedMessage({"nop"});
		}
		
		if(oper.opcode == "chat"){
			if(events.OnChat != nullptr){
				events.OnChat(this, oper);
			}
		}
		
		if(oper.opcode == "turn"){
			// It seems argument 0 to turn (index is first , in websocket message) has the time we will take
			// if argument 1 is more than 1, we are waiting in the queue.
			// If argument 0 and 1 are 0, turn is finished. (fire on turn end)
			// if our username is argument 2, then our turn is the current turn (fire on turn begin)
			// if not, then we are currently waiting argument 0 time.
			
			// This may be horribly wrong
		}
		// TODO (when I'm less lazy): implement more of the event handlers. 
	}

	void VMClient::OnConnect(websocketpp::connection_hdl handle){\
		CVMCPP_DEBUG_INFO("Server connection successful");
		connected = true;
		vm_connhdl = handle;
	}
	
	void VMClient::OnDiscon(websocketpp::connection_hdl handle){
		CVMCPP_DEBUG_INFO("Server connection closed");
		connected = false;
	}

	bool VMClient::Connect(std::string ip) {
			std::string uri = "ws://" + ip;
			try {
				vmclient.clear_access_channels(websocketpp::log::alevel::all);
				vmclient.clear_error_channels(websocketpp::log::elevel::all);
			
				vmclient.init_asio();
				vmclient.set_open_handler(bind(&VMClient::OnConnect, this, ::_1));
				vmclient.set_close_handler(bind(&VMClient::OnDiscon, this, ::_1));
				vmclient.set_message_handler(bind(&VMClient::OnMessage, this, ::_1, ::_2));
	
				websocketpp::lib::error_code ec;
				con = vmclient.get_connection(uri, ec);
				if (ec) {
					CVMCPP_DEBUG_ERROR("WebSocket++ error: " + ec.message());
					return 0;
				}
			
				con->add_subprotocol("guacamole");
				vmclient.connect(con);
				vmthread = websocketpp::lib::make_shared<websocketpp::lib::thread>(&ws_client::run, &vmclient);
			} catch (websocketpp::exception const & e) {
				std::string err_str = "WebSocket++ exception: ";
				err_str += e.what();
				CVMCPP_DEBUG_ERROR(err_str);
				return false;
			}
			return true;
	}
	
	void VMClient::AssignEvents(VMEvents events_toreg) {
		CVMCPP_DEBUG_INFO("Registering events");
		events = events_toreg;
	}
	
	void VMClient::Rename(std::string new_name){
		username = new_name;
		SendEncodedMessage({"rename", {new_name}});
		// TODO: if we get a rename instruction on ourselves (eg: username not accepted/already in use) set server's new name
	}
	
	void VMClient::ConnectVM(std::string name, std::string vm) {
		Rename(name);
		SendEncodedMessage({"connect", {vm}});
	}
	
	void VMClient::TakeTurn(){
		SendEncodedMessage({"turn", {}});
	}
	
	void VMClient::SendChat(std::string body){
		SendEncodedMessage({"chat", {body}});
	}
	
	void VMClient::SendEncodedMessage(guacio::GuacOperation op){
			try { 
				vmclient.send(vm_connhdl, guacio::encode(op), websocketpp::frame::opcode::text);
			} catch (websocketpp::exception const & e) {
				std::string err_str = "WebSocket++ exception: ";
				err_str += e.what();
				CVMCPP_DEBUG_ERROR(err_str);
			}
	}

}
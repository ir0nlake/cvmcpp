#include <cvmcpp/guacio.h>
#include <cvmcpp/html.h>

namespace cvmcpp::guacio {

	// Stolen from c3po (misc-bots), molested into my guacio shit
	// at least ctrl knew how to into guacamole....
	GuacOperation decode(std::string str) {
		std::vector<std::string> decoded;
		std::stringstream ss;
		ss << str;
		for(size_t i = 0; i < str.length();++i) {
			char testChar;
			ss >> testChar;
			int ssCountOriginal = static_cast<int>(ss.tellg()) - 1, wordLength{0};
			switch(testChar) {
				case '0': case '1': case '2': case '3':
				case '4': case '5': case '6': case '7':
				case '8': case '9':
					ss.putback(testChar);
				
					ss >> wordLength;
					
					i += (static_cast<int>(ss.tellg()) - ssCountOriginal) + 1;

					// Sync buffer
					ss >> testChar;
					break;
				default: return {""};
			}
			std::string word("");
			for(size_t k = i;k < i + wordLength;++k) word += ss.get();
			decoded.push_back(word);

			// Advance stream and i one
			i += wordLength + 1;
			ss >> testChar;
		}
		std::vector<std::string> args;
		for(size_t i = 1; i < decoded.size(); i++){
			if(decoded[0] == "chat" && i == 2) {
				// HACK: Push back the unescaped chat message (should I just run everything through unescape)
				args.push_back(html::unescape(decoded[i]));
			} else {
				args.push_back(decoded[i]);
			}
		}
		return {decoded[0], args};
	}


	std::string encode(GuacOperation op){
		std::stringstream strm;
		size_t op_length = strlen(op.opcode.c_str());
		strm << op_length << "." << op.opcode;
	
		for(auto it = op.arguments.begin(); it != op.arguments.end(); ++it){
			std::string arg_seperated = (std::string) *it;
			size_t arg_length = strlen(arg_seperated.c_str());
			strm << ',' << arg_length << "." << arg_seperated;
		}
	
		strm << ";";
		return strm.str();
	}

}
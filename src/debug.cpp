#include <cvmcpp/debug.h>

#ifndef _WIN32
	// For the non-windows party.
	#define RESETCOLOR "\033[0m"
	#define WCOLOR "\033[33;1m"
	#define ECOLOR "\033[31;1m"
#else
	#include <windows.h>
	// read the below "rant" for why these are blank
	#define RESETCOLOR ""
	#define WCOLOR ""
	#define ECOLOR ""
#endif

namespace cvmcpp::debug {
	
	#ifdef _WIN32
		// Windows has to be the spechul snowflake and not take ANSI escape sequences.
		// (at least not a decent version of Windows. 10 can suck my asshole dry)
		// Oh well, this hacky fuck works ok.
		void SetColor(WORD color){
			HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
			SetConsoleTextAttribute(hConsole, color);
		}
	
		void ResetColor(){
			HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
			// TODO: is 7 actually valid
			SetConsoleTextAttribute(hConsole, 7);
		}
	#endif
	
	void debug_log(debug_severity sever, std::string message, int line, char* file) {
		switch(sever){
			default:
			break;
			
			case info:
				std::cout << "[I] " << message << "\n";
			break;
			
			case warning:
				#ifdef _WIN32
					SetColor(14);
				#endif
				std::cout << WCOLOR << "[W] " << message << " (" <<  file << ":" << line << ")\n" << RESETCOLOR;
				#ifdef _WIN32
					ResetColor();
				#endif
			break;
			
			case error:
				#ifdef _WIN32
					SetColor(12);
				#endif
				std::cout << ECOLOR << "[E] " << message << " (" <<  file << ":" << line << ")\n" << RESETCOLOR;
				#ifdef _WIN32
					ResetColor();
				#endif
			break;
			
		}
		
	}
	
}